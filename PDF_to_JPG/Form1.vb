﻿Imports BitMiracle.Docotic.Pdf
Public Class Form1
    ' ./CoderS
    ' - noobcoder : PoetralesanA
    ' - Thanks For : Programmer Vb.NET Indonesia Group (https://m.facebook.com/groups/programervbnetindonesia) | BitMiracle Lib | R2DS | Dhea | Queen | Ways | Densend |

    '-- Social Media --
    '[ Facebook ]
    'facebook.com/poetralesana
    '[ Youtube ]
    'youtube.com/channel/UCesKL9YMfIjKaf7JysGmAQQ/videos
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtOutput.Text = pdf.getPath_OutputFile
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btnOutpufile.Click
        pdf.Open_Output_File()
    End Sub


    Private Sub btn_pilihfile_Click(sender As Object, e As EventArgs) Handles btn_pilihfile.Click
        Dim getpath As String = String.Empty

        getpath = pdf.Pilihfilepdf()
        txtPathfile.Text = getpath


        If getpath <> String.Empty Then
            btnConvert.Enabled = True
            btnOutpufile.Enabled = True
        Else
            btnConvert.Enabled = False
            btnOutpufile.Enabled = False
        End If
    End Sub

    Private Sub bntConvert_Click(sender As Object, e As EventArgs) Handles btnConvert.Click
        Me.Text = "PDF Convert (Please Wait..)"
        If pdf.ConvertPDF() = True Then
            Me.Text = "PDF Convert"
        End If
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Form2.ShowDialog()
    End Sub
End Class
