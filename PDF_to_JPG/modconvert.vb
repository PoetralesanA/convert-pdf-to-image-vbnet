﻿Imports BitMiracle.Docotic.Pdf

Namespace pdf
    ' ./CoderS
    ' - noobcoder : PoetralesanA
    ' - Thanks For : Programmer Vb.NET Indonesia Group (https://m.facebook.com/groups/programervbnetindonesia) | BitMiracle Lib | R2DS | Dhea | Queen | Ways | Densend |

    '-- Social Media --
    '[ Facebook ]
    'facebook.com/poetralesana
    '[ Youtube ]
    'youtube.com/channel/UCesKL9YMfIjKaf7JysGmAQQ/videos
    Module modulconvert
        Private varOutput As String = Application.StartupPath & "\outputconvert"
        Private varPathFile As String = String.Empty

        Public Sub Open_Output_File()
            Process.Start(varOutput)
        End Sub
        Public Function getPath_OutputFile()
            Return varOutput
        End Function
        Public Function Pilihfilepdf()
            varPathFile = String.Empty
            Using ofd As OpenFileDialog = New OpenFileDialog
                ofd.DefaultExt = "pdf"
                ofd.FileName = "PDF File"
                ofd.Filter = "PDF Files|*.PDF"
                ofd.Title = "CARI FILE PDF.."
                If ofd.ShowDialog() <> DialogResult.Cancel Then
                    varPathFile = ofd.FileName
                End If
            End Using
            Return varPathFile
        End Function
        Public Function ConvertPDF() As Boolean
            Dim check As Boolean = False
            Using pdf = New PdfDocument(varPathFile)
                Try
                    Dim options As PdfDrawOptions = PdfDrawOptions.Create()
                    options.BackgroundColor = New PdfRgbColor(255, 255, 255)
                    options.HorizontalResolution = 300
                    options.VerticalResolution = 300

                    Dim i As Integer = 0
                    While i < pdf.PageCount
                        check = False 'proses
                        pdf.Pages(i).Save(varOutput + "\" & "Convertpdfbypoetralesana" & i & ".jpg", options)
                        System.Threading.Interlocked.Increment(i)
                        If i = pdf.PageCount Then
                            check = True 'komplit
                            MessageBox.Show(Form1, "Convert selesai", "Succesfully", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    End While
                Catch ex As Exception
                    MessageBox.Show(Form1, ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Application.Exit()
                End Try

            End Using
            Return check
        End Function
    End Module

End Namespace
